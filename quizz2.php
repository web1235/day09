<head>
 <meta charset="UTF-8" />
 
 <title>MCQs</title>
 
 <link rel="stylesheet" type="text/css" href="style.css" />
</head>
 
<body>
<h1>PHP Multiple-Choice Questions </h1>
 
<?php
        session_start();
    
        $answersQ6 = array("write","php.write","log","echo");
        $answersQ7 = array("local","global","static","external");
        $answersQ8 = array("print","print()","print()","A&B");
        $answersQ9 = array("Integer","Complex","Float","String");
        $answersQ10 = array("1","2","3","4");

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            
            if(isset($_POST["answer6"])&&isset($_POST["answer7"])&&isset($_POST["answer8"])&&isset($_POST["answer9"])&&isset($_POST["answer10"])){
                $answer6 = $_POST["answer6"];
                $answer7 = $_POST["answer7"];
                $answer8 = $_POST["answer8"];
                $answer9 = $_POST["answer9"];
                $answer10 = $_POST["answer10"];
                setcookie("answer6", $answer6, time() + (86400 * 30), "/");
                setcookie("answer7", $answer7, time() + (86400 * 30), "/");
                setcookie("answer8", $answer8, time() + (86400 * 30), "/");
                setcookie("answer9", $answer9, time() + (86400 * 30), "/");
                setcookie("answer10", $answer10, time() + (86400 * 30), "/");
    
               


                header("location: result.php");
            }else{
                echo "<div style='color: red;font-size: 18px;font-weight: bold;text-align: center;'>Hãy nhập trả lời tất cả các câu hỏi</div>";
            }

        }
        
    ?>
            <form name="MCQs" method="post" enctype="multipart/form-data" action="">
                <div class="main">
                    <div class="question_6">
                        <div class="question"> Q6. Which statement is commonly used for PHP output?</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($answersQ6 as $i){
                                    echo"<li>
                                            <input class='tick-question' type='radio' value=".$i." name='answer6'>"
                                            .$i.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                    </div>
                    <div class="question_7">
                        <div class="question">Q7. Which is not a valid variable scope in PHP?</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($answersQ7 as $i){
                                    echo"<li>
                                            <input class='tick-question' type='radio' value=".$i." name='answer7'>"
                                            .$i.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                    </div>
                    <div class="question_8">
                        <div class="question">Q8. What is the correct syntax of echo statement in PHP?</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($answersQ8 as $i){
                                    echo"<li>
                                            <input class='tick-question' type='radio' value=".$i." name='answer8'>"
                                            .$i.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                    </div>
                    <div class="question_9">
                        <div class="question">Q9. What is the correct syntax of print statement in PHP?</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($answersQ9 as $i){
                                    echo"<li>
                                            <input class='tick-question' type='radio' value=".$i." name='answer9'>"
                                            .$i.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                      
                    </div>
                    <div class="question_10">
                        <div class="question">Q10. How many variable scopes are there in PHP?</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($answersQ10 as $i){
                                    echo"<li>
                                            <input class='tick-question' type='radio' value=".$i." name='answer10'>"
                                            .$i.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                       
                    </div>
                    <div class="btn-next">
                        <button >
                           Submit
                        </button>
                    </div>  
                </div>
            </form>


</body>

 
</html>