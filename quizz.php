<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css" />
    <title>MCQs</title>
</head>
    
<body>
    <h1>PHP Multiple-Choice Questions </h1>
    <?php
        session_start();
    
        $answersQ1 = array("1","5","12", "Error");
        $answersQ2 = array(".ph",".html",".xml",".php");
        $answersQ3 = array("1993","1994","1995","1996");
        $answersQ4 = array("semicolon(;)","colon(:)","dot(.)","comma(,)");
        $answersQ5 = array("age","_age","PersonAge","1age");

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            
            if(isset($_POST["answer1"])&&isset($_POST["answer2"])&&isset($_POST["answer3"])&&isset($_POST["answer4"])&&isset($_POST["answer5"])){
                $answer1 = $_POST["answer1"];
                $answer2 = $_POST["answer2"];
                $answer3 = $_POST["answer3"];
                $answer4 = $_POST["answer4"];
                $answer5 = $_POST["answer5"];
                setcookie("answer1", $answer1, time() + (86400 * 30), "/");
                setcookie("answer2", $answer2, time() + (86400 * 30), "/");
                setcookie("answer3", $answer3, time() + (86400 * 30), "/");
                setcookie("answer4", $answer4, time() + (86400 * 30), "/");
                setcookie("answer5", $answer5, time() + (86400 * 30), "/");
    
                
                header("location: quizz2.php");
            }else{
                echo "<div style='color: red;font-size: 18px;font-weight: bold;text-align: center;'>Hãy nhập trả lời tất cả các câu hỏi</div>";
            }

        }
        
    ?>
            <form name="MCQs" method="post" enctype="multipart/form-data" action="">
                <div class="main">
                    <div class="question_1">
                        <div class="question">Q1.  If $a = 12 what will be returned when ($a == 12) ? 5 : 1 is executed?</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($answersQ1 as $i){
                                    
                                    echo "<li> <input class='tick-question' type='radio' value=" . $i . " name='answer1'>" . $i . "</li>";
                                }
                            ?>    
                        </ul>  
                        
                    </div>
                    <div class="question_2">
                        <div class="question">Q2. What is the extension of a PHP file?</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($answersQ2 as $i){
                                    echo"<li>
                                            <input class='tick-question-question' type='radio' value=".$i." name='answer2'>"
                                            .$i.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                       
                    </div>
                    <div class="question_3">
                        <div class="question">Q3. In which year PHP was developed?</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($answersQ3 as $i){
                                    echo"<li>
                                            <input class='tick-question-question' type='radio' value=".$i." name='answer3'>"
                                            .$i.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                        
                    </div>
                    <div class="question_4">
                        <div class="question">Q4. PHP statements end with a ______.</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($answersQ4 as $i){
                                    echo"<li>
                                            <input class='tick-question-question' type='radio' value=".$i." name='answer4'>"
                                            .$i.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                        
                    </div>
                    <div class="question_5">
                        <div class="question">Q5. Which is not a valid variable name in PHP?</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($answersQ5 as $i){
                                    echo"<li>
                                            <input class='tick-question-question' type='radio' value=".$i." name='answer5'>"
                                            .$i.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                       
                    </div>
                    <div class="btn-next">
                        <button >
                            NEXT
                        </button>
                    </div>  
                </div>
            </form>


</body>
